﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;


public partial class RegisterPatient : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["NurseID"] == null) //will redirect immediately if not loged in before
        {
            Response.Redirect("LoginPage.aspx");
        }
        else 
        {
            Label9.Text = Session["NurseID"].ToString();
           

        }
    
    }
    
   // SqlConnection con = new SqlConnection("Data Source=JEVGENIJ-PC;Initial Catalog=Hospital;Integrated Security=True");
  
 /**   private void Fillgrid()
    {
        BLLPatient pt = new BLLPatient();
        GridView1.DataSource = pt.Select_ID();
        GridView1.DataBind();
        pt = null;
    }*/

    private BLLPatient ptn = new BLLPatient(); // creating business logic layer object
    protected void Button1_Click(object sender, EventArgs e)
    {
        
       
        RegisterStatusLbl.Visible = true;
        



      //  ptn.PatientId = int.Parse(TextBox8.Text);
        ptn.FullName = TextBoxFname.Text;
     
        ptn.Address = TextBoxAddress.Text;
        ptn.PhoneNumber = TextBoxPhoneNr.Text;
        ptn.PatientType = RadioButtonList1.Text;
        ptn.birthDate = DateTime.Parse(TextBoxPatientBD.Text);
        ptn.Register();

        
        RegisterStatusLbl.ForeColor = System.Drawing.Color.Green;
        RegisterStatusLbl.Text = "Patient Registered successfully";


        Response.AppendHeader("Refresh", "2;url=SubscribePage.aspx");


        
      
    }

    protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
   
    protected void LogOut(object sender, EventArgs e)
    {
        Session.RemoveAll();
        Response.Redirect("MainMenu.aspx");
    }
    
}