﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;


public partial class UpdatePatient : System.Web.UI.Page
{
    private BLLPatient ptn = new BLLPatient();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["NurseID"] == null) //will redirect immediately if not loged in before
        {
            Response.Redirect("LoginPage.aspx");
        }
        else
        {
            Label12.Text = Session["NurseID"].ToString();


        }
    }

    protected void LogOut(object sender, EventArgs e)
    {
        Session.RemoveAll();
        Response.Redirect("MainMenu.aspx");
    }

   
    protected void Button1_Click(object sender, EventArgs e)
    {


        UpdateStatusLbl.Visible = true;
    



        ptn.PatientId = int.Parse(TextBoxPatientID.Text);
        ptn.FullName = TextBoxPatientName.Text;
        ptn.Address = TextBoxPatientAddress.Text;
        ptn.PhoneNumber = TextBoxPatientPhoneNr.Text;
        ptn.PatientType = RadioButtonList1.Text;
        ptn.birthDate = DateTime.Parse(TextBoxPatientBD.Text);
        ptn.updatePatient();


        UpdateStatusLbl.ForeColor = System.Drawing.Color.Green;
        UpdateStatusLbl.Text = "Patient updated successfully";


        Response.AppendHeader("Refresh", "2;url=TOKSCalcPage.aspx");




    }

    protected void RadioButtonList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        BLLPatient objpl = new BLLPatient();

        //  GridView1.DataSource = objpl.Select_ID();

        
    }

    protected void DropDownList1_TextChanged(object sender, EventArgs e)
    {

        SqlConnection cn = new SqlConnection(@"Data Source=JEVGENIJ-PC\SQLEXPRESS;Initial Catalog=BachelorDB;Integrated Security=True");

        cn.Open();
        SqlCommand cmd = new SqlCommand(@"select * from Patient where Patient_Id= @Pid", cn);
        cmd.Parameters.Add(new SqlParameter("@Pid", DropDownList1.SelectedValue));


        cmd.CommandType = System.Data.CommandType.Text;
        cmd.Connection = cn;

        SqlDataReader dr = cmd.ExecuteReader();

        if (dr.Read())
        {
            TextBoxPatientID.Text = dr[0].ToString(); // 1st col
            TextBoxPatientName.Text = dr[1].ToString(); //2nd col
            TextBoxPatientAddress.Text = dr[2].ToString(); // 3rd col
            TextBoxPatientPhoneNr.Text = dr[3].ToString();   //4th col
            CurrentPatientTypeLbl.Text = dr[4].ToString(); //5th col


        }
        dr.Close();
        cn.Close();
    }
   
}
