﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TOKSCalcPage.aspx.cs" Inherits="TOKSCalcPage" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="/bootstrap/jquery-1.11.3.min.js"></script>
        <script src="/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="/bootstrap/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" href="style.css" />
          <title>Sepsis disease warning project</title>
    </head>

    <body class="body" runat="server">
        <form id="form1" runat="server" class="form-signin">
            <nav class="navbar navbar-inverse">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">midt</a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="MainMenu.aspx">Home</a></li>
                            <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Patient <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                <li><a href="RegisterPatient.aspx">Register patient</a></li>
                                <li><a href="SubscribePage.aspx">Subscribe patient</a></li>
                                <li><a href="NursePatientOverview.aspx">Patient overview</a></li>
                                <li><a href="UpdatePatient.aspx">Edit patient's info</a></li>
                            </ul>
                            </li>
                                <li class="active"><a href="TOKSCalcPage.aspx">Calculate critical TOKS</a></li>
                                <li><a href="Graph.aspx">TOKS evolvement</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#">User ID: <asp:Label ID="SessionLbl" runat="server" Text="Label"></asp:Label></a></li>
                            <li><asp:Button ID="Button1" runat="server" OnClick="LogOut" Text="Logout" CssClass="btn btn-primary LogOutButton" /></li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="container">
                <div class="MainForm">
                    <p>Select Patient:</p>
                    <asp:DropDownList CssClass="form-control" ID="DropDownList1" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource1" DataTextField="FullName" DataValueField="Patient_Id" OnTextChanged="DropDownList1_TextChanged">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:BachelorDBConnectionString %>" SelectCommand="SELECT PatientSubscribe.Patient_Id, PatientSubscribe.Nurse_Id, Patient.FullName FROM PatientSubscribe INNER JOIN Patient ON PatientSubscribe.Patient_Id = Patient.Patient_Id WHERE (PatientSubscribe.Nurse_Id = @Nurse_Id)">
                        <SelectParameters>
                            <asp:SessionParameter Name="Nurse_Id" SessionField="NurseID" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <p>ID:</p>
                    <asp:TextBox CssClass="form-control" ID="TextBoxPatientID" runat="server"></asp:TextBox>
                    <asp:CheckBox ID="CheckBox2" runat="server" AutoPostBack="True" OnCheckedChanged="CheckBox2_CheckedChanged" Text="KOL" />
                    <asp:CheckBox ID="CheckBox3" runat="server" AutoPostBack="True" OnCheckedChanged="CheckBox3_CheckedChanged" Text="AFLI" />
                    <p>Respiratory frequency:</p> 
                    <asp:TextBox CssClass="form-control" ID="TextBoxRespFreq" Text="0" runat="server" onkeypress="return validate(event)"></asp:TextBox>
                    <asp:Label ID="Label4" runat="server" Text="Saturation level:"></asp:Label>
                    <asp:Label ID="Label11" runat="server" Text="(KOL)" Visible="False"></asp:Label>
                    <asp:TextBox CssClass="form-control" ID="TextBoxSatLvlKOL"  text="0" runat="server" onkeypress="return validate(event)"></asp:TextBox>
                    <p>Saturation level KOL:</p>
                    <asp:TextBox CssClass="form-control" ID="TextBoxSatLvl" Text="0" runat="server" onkeypress="return validate(event)"></asp:TextBox>
                    <p>Systolic blood pressure:</p>
                    <asp:TextBox CssClass="form-control" ID="TextBoxBloodPress" Text="0" runat="server" onkeypress="return validate(event)"></asp:TextBox>
                    <p>Pulse:</p>
                    <asp:TextBox CssClass="form-control" ID="TextBoxPulse" Text="0" runat="server" onkeypress="return validate(event)"></asp:TextBox>
                    <p>Puls AFLI:</p>
                    <asp:TextBox CssClass="form-control" ID="TextBoxPulsAFLI" Text="0" runat="server" onkeypress="return validate(event)"></asp:TextBox>
                    <p>Temperature:</p>
                    <asp:TextBox CssClass="form-control" ID="TextBoxTemperature" Text="0" runat="server" onkeypress="return validate(event)"></asp:TextBox>
                    <p>Select Patient's consciousness state</p>
                    <asp:ListBox CssClass="form-control" ID="ListBoxConsLvl" runat="server">
                        <asp:ListItem Value="0">Habituel</asp:ListItem>
                        <asp:ListItem Value="1">agiteret</asp:ListItem>
                        <asp:ListItem Value="2">reagerer kun paa smerte</asp:ListItem>
                        <asp:ListItem Value="3">Ingen reaktion</asp:ListItem>
                    </asp:ListBox>
                    <br />
                    <asp:Button ID="SubmitTOKSCalcBtn" runat="server" OnClick="Button1_Click" Text="Calculate TOKS Score" CssClass="btn btn-success" />
                </div>
            <//div>
        </form>
    </body>
</html>
