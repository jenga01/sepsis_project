﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LoginPage.aspx.cs" Inherits="LoginPage" %>

<!
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/bootstrap/css/bootstrap-theme.min.css" />
    <link rel="stylesheet" href="style.css" />

    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <title>Sepsis disease warning project</title>
</head>
<body class="body">
    <div class="container"> 
        <form class="form-signin LoginForm" runat="server" id="LoginUserForm">
            <asp:Label ID="ErrorLable" runat="server" Text="" CssClass="LoginFormError"></asp:Label>
            <h2 class="form-signin-heading">Please sign in</h2>
            <i>User ID</i>
            <asp:TextBox ID="LoginID" CssClass="form-control" runat="server" TextMode="Number"></asp:TextBox>
            <i>Password</i>
            <asp:TextBox ID="LoginPassword" CssClass="form-control" runat="server" TextMode="Password"></asp:TextBox>
            <br />
            <asp:Button CssClass="btn btn-lg btn-primary btn-block" ID="ButtonLogin" runat="server" OnClick="Button1_Click" Text="Login" />
        </form>
    </div>
</body>
</html>

