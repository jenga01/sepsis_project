﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Configuration;
using System.Security.Cryptography;


/// <summary>
/// Summary description for DataACL
/// </summary>
public class DataACL
{
    SqlConnection con;
    SqlCommand cmd;
    DataACL dl_obj = null;
   
   
    
    //   SqlDataReader dr;


 
    private string GetConnectionString()
    {
        string connectionStr = System.Configuration.ConfigurationManager.ConnectionStrings["MyConnection"].ToString();
        return connectionStr;
    }


    private int NonQuery(SqlCommand cmd) //opens and closes connection, executes non-query statements, gets connection string
    {
        try
        {
            int RowsEffected = 0;

            con = new SqlConnection(GetConnectionString()); // argument is connection string
            cmd.Connection = con;
            con.Open(); //opening connection
            RowsEffected = cmd.ExecuteNonQuery();  //executing non-query
            con.Close(); //closing connection
            return RowsEffected;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }




    public void RegisterPatient(BLLPatient ptn)
    {

        try
        {

            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text; //representing type of command
            cmd.CommandText = "INSERT INTO Patient values(@FullName, @Address, @Phone_number, @Patient_type, @Birth_date)";
          
            cmd.Parameters.AddWithValue("@FullName", ptn.FullName);
            cmd.Parameters.AddWithValue("@Address", ptn.Address);
            cmd.Parameters.AddWithValue("@Phone_number", ptn.PhoneNumber);
            cmd.Parameters.AddWithValue("@Patient_type", ptn.PatientType);
            cmd.Parameters.AddWithValue("@Birth_date", ptn.birthDate);

            dl_obj = new DataACL();
            dl_obj.NonQuery(cmd); //opening and closing connection, executing non-query statements, getting connection string

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void RegisterNurse(Nurse nur)
    {
     
        try
        {
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text; //representing type of command
            cmd.CommandText = "INSERT INTO Nurse values(@Fullname_, @Password, @userType)";
            cmd.Parameters.AddWithValue("@Fullname_", nur.NurseFullName);
            cmd.Parameters.AddWithValue("Password", nur.Password);
            cmd.Parameters.AddWithValue("@userType", nur.userType);


            dl_obj = new DataACL();
            dl_obj.NonQuery(cmd);

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public void update_patient(BLLPatient ptn)
    {

        try
        {

            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text; //representing type of command
            cmd.CommandText = "UPDATE Patient set FullName=@FullName,  Address = @address, Phone_number=@Phone_number, Patient_type = @Patient_Type, Birth_date = @Birth_date where Patient_Id = @Patient_Id";
            cmd.Parameters.AddWithValue("@Patient_Id", ptn.PatientId);
            cmd.Parameters.AddWithValue("@FullName", ptn.FullName);
            
            cmd.Parameters.AddWithValue("@address", ptn.Address);
            cmd.Parameters.AddWithValue("@Phone_number", ptn.PhoneNumber);
            cmd.Parameters.AddWithValue("@Patient_Type", ptn.PatientType);
            cmd.Parameters.AddWithValue("@Birth_date", ptn.birthDate);

            dl_obj = new DataACL();
            dl_obj.NonQuery(cmd);

        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    public void update_nurse(Nurse nur)
    {
        try
        {
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "UPDATE Nurse set userType = @ut where Nurse_Id=@NI";
            cmd.Parameters.AddWithValue("@NI", nur.NurseID);
            cmd.Parameters.AddWithValue("@ut", nur.userType);
            dl_obj = new DataACL();
            dl_obj.NonQuery(cmd);

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    public void CalculateToksScore(BLLTOKS toks)
    {
        try
        {
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text; //representing type of command
            cmd.CommandText = "INSERT INTO TOKS values(@Patient_Id, @Resp_freq, @Saturation, @Saturation_KOL, @Sys_pressure, @Puls, @Puls_AFLI, @Temperature, @ConsLvl, @TOKS_Score, @Alarm, @Last_Measurement_Time, @Next_Measurement_Time)";
            cmd.Parameters.AddWithValue("@Patient_Id", toks.PatientId);
            cmd.Parameters.AddWithValue("@Resp_freq", toks.RespFreq);
            cmd.Parameters.AddWithValue("@Saturation", toks.O2SatLvl);
            cmd.Parameters.AddWithValue("@Saturation_KOL", toks.O2SatLvlKOL);
            cmd.Parameters.AddWithValue("@Sys_pressure", toks.SysBloodPress);
            cmd.Parameters.AddWithValue("@Puls", toks.Puls);
            cmd.Parameters.AddWithValue("@Puls_AFLI", toks.PulsAFLI);
            cmd.Parameters.AddWithValue("@Temperature", toks.Temperature);
            cmd.Parameters.AddWithValue("@ConsLvl", toks.ConsLvl);
            cmd.Parameters.AddWithValue("@TOKS_Score", toks.FinalTOKS);
            cmd.Parameters.AddWithValue("@Alarm", toks.Alert);
            cmd.Parameters.AddWithValue("@Last_Measurement_Time", toks.CurrentMeasurementTime);
            cmd.Parameters.AddWithValue("@Next_Measurement_Time", toks.NextMeasurement);




            dl_obj = new DataACL();
            dl_obj.NonQuery(cmd);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
   

    public void Subscribe(SubscribePTN sbc)
    {

        try
        {

            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text; //representing type of command
            cmd.CommandText = "INSERT INTO PatientSubscribe values(@Patient_Id, @Nurse_ID)";
            //  cmd.Parameters.AddWithValue("@Patient_Id", sbc.NurseID);
            cmd.Parameters.AddWithValue("@Patient_Id", sbc.PatientId);
            cmd.Parameters.AddWithValue("@Nurse_ID", sbc.NurseID);
          //  cmd.Parameters.AddWithValue("@Shift", sbc.Shift);


            dl_obj = new DataACL();
            dl_obj.NonQuery(cmd);

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void Unsubscribe(SubscribePTN unsbs)
    {
        try
        {
            cmd = new SqlCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "Delete from  PatientSubscribe where Patient_Id = @PID AND Nurse_Id=@NI";
            cmd.Parameters.AddWithValue("@PID", unsbs.PatientId);
            cmd.Parameters.AddWithValue("@NI", unsbs.NurseID);
            dl_obj = new DataACL();
            dl_obj.NonQuery(cmd);

        }
        catch (Exception ex)
        {
            throw ex;
        }
    

    }
    public  int userLogin(int NiD, string password)
    {
       
        SqlConnection con = new SqlConnection();
        con.ConnectionString = GetConnectionString();
        con.Open();
        int id = 0;
        SqlCommand cmd = new SqlCommand(@"SELECT Nurse_ID, Password, userType FROM Nurse WHERE Nurse_ID = @user AND password = @pw AND userType=1", con); //allows login only if userType is 1, in database type set as BIT(0=0, everything that is 1=< is stored as 1)

        cmd.Parameters.Add(new SqlParameter("@user", NiD));
        cmd.Parameters.Add(new SqlParameter("@pw", password));
    
        cmd.CommandType = System.Data.CommandType.Text;
        cmd.Connection = con;
        SqlDataReader reader = cmd.ExecuteReader();
        while (reader.Read())
        {
            id++;
        }
        if (reader.Read())
        {

        }
        cmd = null;
        reader.Close();
        con.Close();
        return id;
    }
    public int adminLogin(string adminName, string password){


        SqlConnection con = new SqlConnection();
        con.ConnectionString = GetConnectionString();
        con.Open();
        int id = 0;
        SqlCommand cmd = new SqlCommand(@"SELECT adminName, Password FROM Administrator WHERE adminName = @an AND Password = @pw", con);

        cmd.Parameters.Add(new SqlParameter("@an", adminName));
        cmd.Parameters.Add(new SqlParameter("@pw", password));

        cmd.CommandType = System.Data.CommandType.Text;
        cmd.Connection = con;
        SqlDataReader reader = cmd.ExecuteReader();
        while (reader.Read())
        {
            id++;
        }
        if (reader.Read())
        {

        }
        cmd = null;
        reader.Close();
        con.Close();
        return id;
    }
   
}
    
    