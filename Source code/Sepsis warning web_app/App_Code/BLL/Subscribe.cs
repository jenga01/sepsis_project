﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Subscribe
/// </summary>
public class SubscribePTN
{

    public int PatientId { set; get; }
    public int NurseID { set; get; }

    // public char Shift { set; get; }
    private DataACL acl = new DataACL(); //access DataACL object only from this class
    public void SubscribePatient()
    {
        try
        {

            acl.Subscribe(this);
        }
        catch (Exception ex)
        {
            throw new Exception("This patient is already subscribed.", ex);
        }
    }

    public void UnsubscribePatient()
    {
        try
        {
            acl.Unsubscribe(this);
        }

        catch (Exception ex)
        {
            throw ex;
        }

    }
}