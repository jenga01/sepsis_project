﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Xml;

/// <summary>
/// Summary description for ToksCalc
/// </summary>
public class BLLTOKS
{
    XmlNodeList TOKSValues;
    public int PatientId { get; set; }
    public int RespFreq { get; set; } //Parameters needed to be passed to data access layer
    public int O2SatLvl { set; get; }
    public int O2SatLvlKOL { set; get; }
    public int SysBloodPress { set; get; }
    public int Puls { set; get; }
    public int PulsAFLI { set; get; }
    public double Temperature { set; get; }

    public int ConsLvl { set; get; }



    public int FinalTOKS { set; get; }
    public int FinalTOKSFor1 { set; get; } //
    public int Alert { set; get; }

   private int alertGreen = 0;
    private int alertYellow = 1;
    private int alertYellow2 = 2;
    private int alertOrange = 4;
    private int alertRed = 5;

    private int Toks0 = 0;
    private int Toks1 = 1;
    private int Toks2 = 2;
    private int Toks3 = 3;

    public DateTime CurrentMeasurementTime = DateTime.Now;
   public DateTime NextMeasurement;
 
   public BLLTOKS()
   {

       XmlTextReader xmlreader = new XmlTextReader(@"C:\Users\rt\Documents\Visual Studio 2013\WebSites\WebSite5\TOKSxml.xml");
       XmlDocument doc = new XmlDocument();
       doc.Load(xmlreader);
       TOKSValues = doc.GetElementsByTagName("variable");
       //testc();
   }

   private DataACL toks_obj = new DataACL(); //Making sure data access layer object will be accessed only in this class
    
   
        
        public void CalculateRespFrequency()
        {
           
            {
               
                if ( RespFreq >= Convert.ToInt32(TOKSValues[0].LastChild.InnerText) && RespFreq <= Convert.ToInt32(TOKSValues[1].LastChild.InnerText))
                {
                    FinalTOKS = FinalTOKS + Toks0;

                }
                else if (RespFreq >= Convert.ToInt32(TOKSValues[2].LastChild.InnerText) && RespFreq <= Convert.ToInt32(TOKSValues[3].LastChild.InnerText))
                {

                    FinalTOKSFor1 = FinalTOKSFor1 + Toks1;

                }
                else if (RespFreq >= Convert.ToInt32(TOKSValues[4].LastChild.InnerText) && RespFreq <= Convert.ToInt32(TOKSValues[5].LastChild.InnerText))
                {


                    FinalTOKS = FinalTOKS + Toks2;

                }
                else if (RespFreq <= Convert.ToInt32(TOKSValues[6].LastChild.InnerText) || RespFreq >= Convert.ToInt32(TOKSValues[7].LastChild.InnerText))
                {

                    FinalTOKS = FinalTOKS + Toks3;
                }
                // TOKSAlert0();
            }
        }
    public void CalculateO2Sat()
    {
        if (O2SatLvl == 0)
        {
            FinalTOKS = FinalTOKS + Toks0;
        }


        else if (O2SatLvl >= Convert.ToInt32(TOKSValues[8].LastChild.InnerText))
        {

            FinalTOKS = FinalTOKS + Toks0;
        }

        else if (O2SatLvl >= Convert.ToInt32(TOKSValues[9].LastChild.InnerText) && O2SatLvl <= Convert.ToInt32(TOKSValues[10].LastChild.InnerText))
        {

            FinalTOKSFor1 = FinalTOKSFor1 + Toks1;
        }

        else if (O2SatLvl >= Convert.ToInt32(TOKSValues[11].LastChild.InnerText) && O2SatLvl <= Convert.ToInt32(TOKSValues[12].LastChild.InnerText))
        {

            FinalTOKS = FinalTOKS + Toks2;
        }
        else if (O2SatLvl <= Convert.ToInt32(TOKSValues[13].LastChild.InnerText))
        {

            FinalTOKS = FinalTOKS + Toks3;
        }
        
    }
    public void CalculateO2SatKol()
    {

        if (O2SatLvlKOL == 0)
        {
            FinalTOKS = FinalTOKS + Toks0;
        }


        else if (O2SatLvlKOL >= Convert.ToInt32(TOKSValues[14].LastChild.InnerText))
        {
            FinalTOKS = FinalTOKS + Toks0;
        }
        else if (O2SatLvlKOL >= Convert.ToInt32(TOKSValues[15].LastChild.InnerText) && O2SatLvlKOL <= Convert.ToInt32(TOKSValues[16].LastChild.InnerText))
        {
            FinalTOKSFor1 = FinalTOKSFor1 + Toks1;
        }
        else if (O2SatLvlKOL >= Convert.ToInt32(TOKSValues[17].LastChild.InnerText) && O2SatLvlKOL <= Convert.ToInt32(TOKSValues[18].LastChild.InnerText))
        {
            FinalTOKS = FinalTOKS + Toks2;
        }

        else if (O2SatLvlKOL <= Convert.ToInt32(TOKSValues[19].LastChild.InnerText))
        {

            FinalTOKS = FinalTOKS + Toks3;
        }
      

    }
    public void CalculateSBT()
    {
        if (SysBloodPress >= Convert.ToInt32(TOKSValues[20].LastChild.InnerText) && SysBloodPress <= Convert.ToInt32(TOKSValues[21].LastChild.InnerText))
        {

            FinalTOKS = FinalTOKS + Toks0;
        }
        else if (SysBloodPress >= Convert.ToInt32(TOKSValues[22].LastChild.InnerText) && SysBloodPress <= Convert.ToInt32(TOKSValues[23].LastChild.InnerText))
        {
            FinalTOKSFor1 = FinalTOKSFor1 + Toks1;
        }
        else if (SysBloodPress >= Convert.ToInt32(TOKSValues[24].LastChild.InnerText) && SysBloodPress <= Convert.ToInt32(TOKSValues[25].LastChild.InnerText) || SysBloodPress >= Convert.ToInt32(TOKSValues[26].LastChild.InnerText))
        {
            FinalTOKS = FinalTOKS + Toks2;
        }
        else if (SysBloodPress <=Convert.ToInt32(TOKSValues[27].LastChild.InnerText))
        {

            FinalTOKS = FinalTOKS + Toks3;
        }
        
    }
    public void CalculatePuls()
    {
        if (Puls == 0)
        {
            FinalTOKS = FinalTOKS + Toks0;
        }

        else if (Puls >= Convert.ToInt32(TOKSValues[28].LastChild.InnerText) && Puls <= Convert.ToInt32(TOKSValues[29].LastChild.InnerText))
        {

            FinalTOKS = FinalTOKS + Toks0;
        }
        else if (Puls >= Convert.ToInt32(TOKSValues[30].LastChild.InnerText) && Puls <= Convert.ToInt32(TOKSValues[31].LastChild.InnerText) || Puls >= Convert.ToInt32(TOKSValues[32].LastChild.InnerText) && Puls <= Convert.ToInt32(TOKSValues[33].LastChild.InnerText))
        {

            FinalTOKSFor1 = FinalTOKSFor1 + Toks1;
        }
        else if (Puls <= Convert.ToInt32(TOKSValues[34].LastChild.InnerText) || Puls >= Convert.ToInt32(TOKSValues[35].LastChild.InnerText) && Puls <= Convert.ToInt32(TOKSValues[36].LastChild.InnerText))
        {
            FinalTOKS = FinalTOKS + Toks2;
        }
        else if (Puls >= Convert.ToInt32(TOKSValues[37].LastChild.InnerText))
        {

            FinalTOKS = FinalTOKS + Toks3;
        }
        // TOKSAlert0();

    }
    public void CalculatePulsAFLI()
    {
        if (PulsAFLI == 0)
        {
            FinalTOKS = FinalTOKS + Toks0;
        }

        else if (PulsAFLI >= Convert.ToInt32(TOKSValues[38].LastChild.InnerText) && PulsAFLI <= Convert.ToInt32(TOKSValues[39].LastChild.InnerText))
        {
            FinalTOKS = FinalTOKS + Toks0;
        }
        else if (PulsAFLI >= Convert.ToInt32(TOKSValues[40].LastChild.InnerText) && PulsAFLI <= Convert.ToInt32(TOKSValues[41].LastChild.InnerText) || PulsAFLI >= Convert.ToInt32(TOKSValues[42].LastChild.InnerText) && PulsAFLI <= Convert.ToInt32(TOKSValues[43].LastChild.InnerText))
        {
            FinalTOKSFor1 = FinalTOKSFor1 + Toks1;
        }
        else if (PulsAFLI <= Convert.ToInt32(TOKSValues[44].LastChild.InnerText) || PulsAFLI >= Convert.ToInt32(TOKSValues[45].LastChild.InnerText) && PulsAFLI <= Convert.ToInt32(TOKSValues[46].LastChild.InnerText))
        {
            FinalTOKS = FinalTOKS + Toks2;
        }
        else if (PulsAFLI >= Convert.ToInt32(TOKSValues[47].LastChild.InnerText))
        {
            FinalTOKS = FinalTOKS + Toks3;
        }
       
    }

    public void CalculateTemp()
    {
        if (Temperature >= Convert.ToDouble(TOKSValues[48].LastChild.InnerText) && Temperature <= Convert.ToDouble(TOKSValues[49].LastChild.InnerText))
        {
            FinalTOKS = FinalTOKS + Toks0;
        }
        else if (Temperature >= Convert.ToDouble(TOKSValues[50].LastChild.InnerText) && Temperature <= Convert.ToDouble(TOKSValues[51].LastChild.InnerText))
        {
            FinalTOKSFor1 = FinalTOKSFor1 + Toks1;
        }
        else if (Temperature >= Convert.ToDouble(TOKSValues[52].LastChild.InnerText) && Temperature <= Convert.ToDouble(TOKSValues[53].LastChild.InnerText) || Temperature >= Convert.ToDouble(TOKSValues[54].LastChild.InnerText) && Temperature <= Convert.ToDouble(TOKSValues[55].LastChild.InnerText))
        {
            FinalTOKS = FinalTOKS + Toks2;
        }
        else if (Temperature < Convert.ToDouble(TOKSValues[56].LastChild.InnerText) || Temperature >= Convert.ToDouble(TOKSValues[57].LastChild.InnerText))
        {
            FinalTOKS = FinalTOKS + Toks3;
        }
        
    }

    public void consLevel()
    {
        if (ConsLvl == 0)
        {
            FinalTOKS = FinalTOKS + Toks0;
        }

        else if (ConsLvl == 1)
        {
            FinalTOKSFor1 = FinalTOKSFor1 + Toks1;
        }
        else if (ConsLvl == 2)
        {
            FinalTOKS = FinalTOKS + Toks2;
        }

        else if (ConsLvl == 3)
        {

            FinalTOKS = FinalTOKS + Toks3;
        }

       
    }
   
    
    public void TOKSAlert()
    {
        FinalTOKS = FinalTOKS + FinalTOKSFor1;

        if (FinalTOKS == 0)
        {
            Alert = alertGreen;
            NextMeasurement = CurrentMeasurementTime.AddHours(Convert.ToDouble(TOKSValues[58].LastChild.InnerText));

        }

        else if (FinalTOKS == 1)
        {
            Alert = alertYellow;
            NextMeasurement = CurrentMeasurementTime.AddHours(Convert.ToDouble(TOKSValues[59].LastChild.InnerText));
            

        }
        else if (FinalTOKSFor1 == 2)
        {
            Alert = alertYellow2;
            NextMeasurement = CurrentMeasurementTime.AddHours(Convert.ToDouble(TOKSValues[60].LastChild.InnerText));
        }
        else if (FinalTOKS >= 2 && FinalTOKS <= 4)
        {
            Alert = alertOrange;
            NextMeasurement = CurrentMeasurementTime.AddHours(0);
        }
        else if (FinalTOKS >= 5)
        {
            Alert = alertRed;
            NextMeasurement = CurrentMeasurementTime.AddHours(0);
        }
       

         
        toks_obj.CalculateToksScore(this);
    }

    
    }

