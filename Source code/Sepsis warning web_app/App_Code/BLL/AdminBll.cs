﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for AdminBll
/// </summary>
public class AdminLogin
{
   

    private DataACL acl = new DataACL();
    public int adminLog(string admin, string pass)
    {
        try
        {
            return acl.adminLogin(admin, pass);
        }

        catch (Exception ex)
        {
            throw ex;
        }
    }
}