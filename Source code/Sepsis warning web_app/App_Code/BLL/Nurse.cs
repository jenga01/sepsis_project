﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for Nurse
/// </summary>
public class Nurse
{
    public int NurseID { set; get; }
    public string NurseFullName { set; get; }
    public string Password { set; get; }
    public string userType { set; get; }
	
    private DataACL acl = new DataACL();
    public void SetupNurse()
    {
        acl.RegisterNurse(this);
        try
        {

            acl.RegisterNurse(this);
        }
        catch (Exception ex)
        {
            throw new Exception("Nurse setup unsuccessful. Try again", ex);
        }
    }
    public void UpdateNurse()
    {
        acl.update_nurse(this);
    }
}