﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


/// <summary>
/// Summary description for Patient
/// </summary>
public class BLLPatient
{
    public int PatientId { set; get; }
    public string FullName { set; get; } //Parameters needed to be passed to data access layer
   
    public string Address { set; get; }
    public string PhoneNumber { set; get; }
    public string PatientType { set; get; }
    public DateTime birthDate { set; get; }

    private DataACL lo_obj = new DataACL();
	public void Register()
	{
        try
        {
            // call data layer function
            lo_obj.RegisterPatient(this);
        }
      
        catch (Exception ex)
        {
            throw ex;
        }
      
	}
//   private DataACL seobj = new DataACL();
    public void updatePatient()
    {
        try
        {
            lo_obj.update_patient(this);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}