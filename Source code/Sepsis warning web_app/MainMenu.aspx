﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MainMenu.aspx.cs" Inherits="MainMenu" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="/bootstrap/jquery-1.11.3.min.js"></script>
        <script src="/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="/bootstrap/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" href="style.css" />
         <title>Sepsis disease warning project</title>
    </head>

    <body class="body" runat="server">
        <form id="form1" runat="server">
            <nav class="navbar navbar-inverse">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">midt</a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="MainMenu.aspx">Home <span class="sr-only">(current)</span></a></li>
                            <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Patient <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                <li><a href="RegisterPatient.aspx">Register patient</a></li>
                                <li><a href="SubscribePage.aspx">Subscribe patient</a></li>
                                <li><a href="NursePatientOverview.aspx">Patient overview</a></li>
                                <li><a href="UpdatePatient.aspx">Edit patient's info</a></li>
                            </ul>
                            </li>
                                <li><a href="TOKSCalcPage.aspx">Calculate critical TOKS</a></li>
                                <li><a href="Graph.aspx">TOKS evolvement</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#">User ID: <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label></a></li>
                            <li><asp:Button ID="Button1" runat="server" OnClick="LogOut" Text="Logout" CssClass="btn btn-primary LogOutButton" /></li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="container">
                <div class="col-md-6">
                    <p>My Patients</p>
                    <asp:ListBox CssClass="form-control" ID="ListBox1" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource2" DataTextField="FullName" DataValueField="Patient_Id" OnTextChanged="ListBox1_TextChanged" ></asp:ListBox>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:BachelorDBConnectionString %>" SelectCommand="SELECT TOKS.Patient_Id, TOKS.Last_Measurement_Time, TOKS.Next_Measurement_Time, Patient.FullName FROM TOKS INNER JOIN Patient ON TOKS.Patient_Id = Patient.Patient_Id WHERE (TOKS.Patient_Id = @Patient_Id) ORDER BY [Next_Measurement_Time] DESC">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="ListBox1" Name="Patient_Id" PropertyName="SelectedValue" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
                <div class="col-md-6">
                    <p>Next Measurement</p>
                    <asp:ListBox CssClass="form-control" ID="ListBox2" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource1" DataTextField="Next_Measurement_Time" DataValueField="Next_Measurement_Time" OnTextChanged="ListBox2_TextChanged"></asp:ListBox>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:BachelorDBConnectionString %>" SelectCommand="SELECT Patient.FullName, PatientSubscribe.Nurse_Id, PatientSubscribe.Patient_Id FROM Patient INNER JOIN PatientSubscribe ON Patient.Patient_Id = PatientSubscribe.Patient_Id WHERE (PatientSubscribe.Nurse_Id = @Nurse_Id)">
                        <SelectParameters>
                            <asp:SessionParameter Name="Nurse_Id" SessionField="NurseID" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
                <div class="col-md-12">
                    <asp:Label ID="Listbox1SelectedValueLbl" runat="server" Visible="False"></asp:Label>
                </div>
                <div class="cold md-md-12">
                    <asp:Label ID="Label5" runat="server" Font-Bold="True" Text="Time remaining:" Visible="False"></asp:Label>
                    <asp:Label ID="TimeRemLbl" runat="server" Text="Label" Visible="False"></asp:Label>
                    <asp:Label ID="ListBox2SelectedValue" runat="server" Text="Label" Visible="False"></asp:Label>
                </div>
            </div>
        </form>
    </body>
</html>
