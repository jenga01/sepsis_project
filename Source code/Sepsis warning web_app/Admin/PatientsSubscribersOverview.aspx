﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PatientsSubscribersOverview.aspx.cs" Inherits="Admin_PatientsSubscribersOverview" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Admin/AdminMenu.aspx">Admin Menu</asp:HyperLink>
        <br />
    
        Nurses:<br />
        <br />
        <asp:ListBox ID="ListBox1" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource1" DataTextField="Fullname_" DataValueField="Nurse_Id"></asp:ListBox>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:BachelorDBConnectionString %>" SelectCommand="SELECT [Nurse_Id], [Fullname_] FROM [Nurse]"></asp:SqlDataSource>
        <br />
        <br />
        Subscribes to:<br />
        <br />
        <asp:ListBox ID="ListBox2" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource2" DataTextField="FullName" DataValueField="Patient_Id"></asp:ListBox>
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:BachelorDBConnectionString %>" SelectCommand="SELECT PatientSubscribe.Nurse_Id, PatientSubscribe.Patient_Id, Patient.FullName FROM PatientSubscribe INNER JOIN Patient ON PatientSubscribe.Patient_Id = Patient.Patient_Id WHERE (PatientSubscribe.Nurse_Id = @Nurse_Id)">
            <SelectParameters>
                <asp:ControlParameter ControlID="ListBox1" Name="Nurse_Id" PropertyName="SelectedValue" Type="Int32" />
            </SelectParameters>
        </asp:SqlDataSource>
        <br />
    
    </div>
    </form>
</body>
</html>
