﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_AssignPatient : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    private SubscribePTN ptn = new SubscribePTN();
    protected void DropDownList1_TextChanged(object sender, EventArgs e)
    {
        PatientIDTxtBox.Text = DropDownList1.SelectedValue.ToString();
    }
    protected void DropDownList2_TextChanged(object sender, EventArgs e)
    {
        NurseIDTxtBox.Text = DropDownList2.SelectedValue.ToString();
    }
    protected void Submit_Click(object sender, EventArgs e)
    {
        ptn.PatientId = int.Parse(PatientIDTxtBox.Text);
        ptn.NurseID = int.Parse(NurseIDTxtBox.Text);
        StatusLbl.Visible = true;
        StatusLbl.Text = "Patient assigned successfully";
        try
        {

            ptn.SubscribePatient();
        }
        catch (Exception ex)
        {
           // throw new Exception("Patient at this time is already subscribed", ex);
            StatusLbl.Text = ex.Message;
        }
        
    }
}