﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UnsubscribePatients.aspx.cs" Inherits="Admin_UnsubscribePatients" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Admin/AdminMenu.aspx">Admin Menu</asp:HyperLink>
        <br />
        Select Nurse:<br />
        <asp:ListBox ID="ListBox1" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource3" DataTextField="Fullname_" DataValueField="Nurse_Id" OnTextChanged="ListBox1_TextChanged"></asp:ListBox>
        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:BachelorDBConnectionString %>" SelectCommand="SELECT [Nurse_Id], [Fullname_] FROM [Nurse]"></asp:SqlDataSource>
        <br />
        Select Patient:<br />
        <asp:ListBox ID="ListBox2" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource4" DataTextField="FullName" DataValueField="Patient_Id" OnTextChanged="ListBox2_TextChanged"></asp:ListBox>
        <asp:SqlDataSource ID="SqlDataSource4" runat="server" ConnectionString="<%$ ConnectionStrings:BachelorDBConnectionString %>" SelectCommand="SELECT PatientSubscribe.Nurse_Id, PatientSubscribe.Patient_Id, Patient.FullName FROM PatientSubscribe INNER JOIN Patient ON PatientSubscribe.Patient_Id = Patient.Patient_Id WHERE (PatientSubscribe.Nurse_Id = @Nurse_Id)">
            <SelectParameters>
                <asp:ControlParameter ControlID="ListBox1" Name="Nurse_Id" PropertyName="SelectedValue" />
            </SelectParameters>
        </asp:SqlDataSource>
        <br />
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:BachelorDBConnectionString %>" SelectCommand="SELECT [Patient_Id], [FullName] FROM [Patient]"></asp:SqlDataSource>
        <br />
        <br />
        <br />
        <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:BachelorDBConnectionString %>" SelectCommand="SELECT [Nurse_Id], [Fullname_] FROM [Nurse]"></asp:SqlDataSource>
        <br />
        <asp:TextBox ID="PatientIDTxtBox" runat="server" Visible="False"></asp:TextBox>
        <br />
        <asp:TextBox ID="NurseIDTxtBox" runat="server" Visible="False"></asp:TextBox>
        <br />
        <br />
        <br />
        <asp:Button ID="SubmitBtn" runat="server" OnClick="Submit_Click" Text="Unsubscribe " />
    
        <asp:Label ID="PtnUnsubStatusLbl" runat="server" Font-Bold="True" Font-Size="X-Large" Text="Label" Visible="False"></asp:Label>
    
    </div>
    </form>
</body>
</html>
