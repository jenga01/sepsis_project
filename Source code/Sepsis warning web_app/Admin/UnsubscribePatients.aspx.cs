﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_UnsubscribePatients : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    private SubscribePTN unsub = new SubscribePTN();
    protected void Submit_Click(object sender, EventArgs e)
    {
        PtnUnsubStatusLbl.Visible = true;
        unsub.PatientId = int.Parse(NurseIDTxtBox.Text);
        unsub.NurseID = int.Parse(PatientIDTxtBox.Text);

       

        unsub.UnsubscribePatient();
        ;
        PtnUnsubStatusLbl.ForeColor = System.Drawing.Color.Green;
        PtnUnsubStatusLbl.Text = "Patient unsubscribed successfully";

       Response.AppendHeader("Refresh", "1");
    }
    
    protected void ListBox1_TextChanged(object sender, EventArgs e)
    {
        PatientIDTxtBox.Text = ListBox1.SelectedValue.ToString();
    }
    protected void ListBox2_TextChanged(object sender, EventArgs e)
    {
        NurseIDTxtBox.Text = ListBox2.SelectedValue.ToString();
    }
}