﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AdminMenu.aspx.cs" Inherits="Admin_AdminMenu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="height: 522px; width: 1575px">
    
        <asp:Label ID="SessionLbl" runat="server" Text="Label"></asp:Label>
    
        <br />
        <asp:Button ID="LogOutBtn" runat="server" OnClick="LogOutBtn_Click" Text="Logout" />
    
        <br />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="NurseSetBtn" runat="server" Height="155px" Text="Setup new nurse" Width="405px" OnClick="NurseSetBtn_Click" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="SubsOverBtn" runat="server" Height="155px" Text="Patient Subscribers Overview" Width="405px" OnClick="SubsOverBtn_Click" />
&nbsp;&nbsp;&nbsp;
        <asp:Button ID="SendLogDetBtn" runat="server" Height="155px" Text="Send Login Details" Width="405px" OnClick="SendLogDetBtn_Click" />
        <br />
        <br />
    
        <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="EDNurse" runat="server" Height="152px" OnClick="EDNurse_Click" Text="Enable/Disable Nurse" Width="405px" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="AssignPatient" runat="server" Height="146px" OnClick="AssignPatient_Click" Text="Assign patient to a Nurse" Width="410px" />
&nbsp;&nbsp;
        <asp:Button ID="UnassignPtn" runat="server" Height="147px" OnClick="Button2_Click" Text="Unassign Patients from Nurses" Width="405px" />
    
    </div>
    </form>
</body>
</html>
