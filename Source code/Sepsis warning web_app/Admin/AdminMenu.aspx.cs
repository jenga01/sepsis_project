﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_AdminMenu : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["admin"] == null) //will redirect immediately if not loged in before
        {
            Response.Redirect("AdminLogin.aspx");
        }
        else
        {
            SessionLbl.Text = Session["admin"].ToString();
        }

    }
    protected void NurseSetBtn_Click(object sender, EventArgs e)
    {
        Response.Redirect("SetupNurse.aspx");
    }
    protected void SubsOverBtn_Click(object sender, EventArgs e)
    {
        Response.Redirect("PatientsSubscribersOverview.aspx");
    }
    protected void SendLogDetBtn_Click(object sender, EventArgs e)
    {
        Response.Redirect("SendLoginDetails.aspx");
    }
    protected void EDNurse_Click(object sender, EventArgs e)
    {
        Response.Redirect("EnableDisableNurse.aspx");
    }
    protected void AssignPatient_Click(object sender, EventArgs e)
    {
        Response.Redirect("AssignPatient.aspx");
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        Response.Redirect("UnsubscribePatients.aspx");
    }
    protected void LogOutBtn_Click(object sender, EventArgs e)
    {
        Session.RemoveAll();
        Response.Redirect("AdminMenu.aspx");
    }
}