﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class NursePatientOverview : System.Web.UI.Page
{
    //BLLTOKS tks = new BLLTOKS();



    protected void Page_Load(object sender, EventArgs e)
    {
        Label2.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm");
        //TextBox2.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm");
        if (Session["NurseID"] == null) //will redirect immediately if not loged in before
        {
            Response.Redirect("LoginPage.aspx");
        }
        else
        {
            SessionLbl.Text = Session["NurseID"].ToString();



        }

    }




    protected void DropDownList2_TextChanged(object sender, EventArgs e)
    {

        Label2.Text = DropDownList2.SelectedValue.ToString();
        DateTime tr = DateTime.Parse(Label2.Text);
        TimeSpan ts = new TimeSpan(0, 0, 0);
        TimeSpan sincelast = tr - DateTime.Now;
        Label3.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm");
        Label3.Text = sincelast.ToString();

        if (sincelast < ts)
        {
            Label3.ForeColor = System.Drawing.Color.Red;
            Label3.Text = "Completed";
        }
        else
        {
            Label3.ForeColor = System.Drawing.Color.Black;
        }
    }
    protected void DropDownList1_TextChanged(object sender, EventArgs e)
    {

    }

    protected void LogOut(object sender, EventArgs e)
    {
        Session.RemoveAll();
        Response.Redirect("MainMenu.aspx");
    }
}
