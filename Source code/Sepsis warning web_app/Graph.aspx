﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Graph.aspx.cs" Inherits="TestPAGE" %>

<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="/bootstrap/jquery-1.11.3.min.js"></script>
        <script src="/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="/bootstrap/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" href="style.css" />
  <title>Sepsis disease warning project</title>
    </head>

    <body class="body" runat="server">
        <form id="form1" runat="server" class="form-signin">
            <nav class="navbar navbar-inverse">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">midt</a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="MainMenu.aspx">Home <span class="sr-only">(current)</span></a></li>
                            <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Patient <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                <li><a href="RegisterPatient.aspx">Register patient</a></li>
                                <li><a href="SubscribePage.aspx">Subscribe patient</a></li>
                                <li><a href="NursePatientOverview.aspx">Patient overview</a></li>
                                <li><a href="UpdatePatient.aspx">Edit patient's info</a></li>
                            </ul>
                            </li>
                                <li><a href="TOKSCalcPage.aspx">Calculate critical TOKS</a></li>
                                <li class="active"><a href="Graph.aspx">TOKS evolvement</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#">User ID: <asp:Label ID="Label2" runat="server" Font-Bold="True" Text=""></asp:Label></a></li>
                            <li><asp:Button ID="Button1" runat="server" OnClick="LogOut" Text="Logout" CssClass="btn btn-primary LogOutButton" /></li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="container">
                <div class="MainFormGraph">
                    <asp:DropDownList CssClass="form-control" ID="DropDownList1" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource3" DataTextField="FullName" DataValueField="Patient_Id" OnTextChanged="DropDownList1_TextChanged">
                    </asp:DropDownList>
                    <br />
                    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:BachelorDBConnectionString %>" SelectCommand="SELECT PatientSubscribe.Patient_Id, PatientSubscribe.Nurse_Id, Patient.FullName FROM PatientSubscribe INNER JOIN Patient ON PatientSubscribe.Patient_Id = Patient.Patient_Id WHERE (PatientSubscribe.Nurse_Id = @Nurse_Id)">
                        <SelectParameters>
                            <asp:SessionParameter Name="Nurse_Id" SessionField="NurseID" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:BachelorDBConnectionString %>" SelectCommand="SELECT [Patient_Id], [Nurse_Id] FROM [PatientSubscribe] WHERE ([Nurse_Id] = @Nurse_Id)">
                        <SelectParameters>
                            <asp:SessionParameter Name="Nurse_Id" SessionField="NurseID" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                    <asp:TextBox ID="TextBox1" runat="server" Visible="False"></asp:TextBox>
                    <asp:Chart ID="Chart1" runat="server" DataSourceID="SqlDataSource1" DataMember="DefaultView" Height="416px" Width="1107px">
                        <series>
                            <asp:Series Name="Series1" XValueMember="Last_Measurement_Time" YValueMembers="TOKS_Score" ChartType="Line" YValuesPerPoint="2">
                            </asp:Series>
                        </series>
                        <chartareas>
                            <asp:ChartArea Name="ChartArea1">
                            </asp:ChartArea>
                        </chartareas>
                    </asp:Chart>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:BachelorDBConnectionString %>" SelectCommand="SELECT TOKS_Score, Last_Measurement_Time, Next_Measurement_Time, Patient_Id FROM TOKS WHERE (Patient_Id = @Patient_Id)">
                        <SelectParameters>
                            <asp:ControlParameter ControlID="DropDownList1" DefaultValue="" Name="Patient_Id" PropertyName="SelectedValue" Type="Int32" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>
            </div>
        </form>
    </body>
</html>
