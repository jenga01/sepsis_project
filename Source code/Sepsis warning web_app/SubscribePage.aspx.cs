﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SubscribePage : System.Web.UI.Page
{
    private SubscribePTN subs = new SubscribePTN();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["NurseID"] == null) //will redirect immediately if not loged in before
        {
            Response.Redirect("LoginPage.aspx");
        }
        else 
        {
            Label2.Text = Session["NurseID"].ToString();
           

        
    }
    }
    protected void SubButton_Click(object sender, EventArgs e)
    {
        subs.NurseID = int.Parse(Label2.Text);
        subs.PatientId = int.Parse(TextBoxSelectedPatientID.Text);
        SubscribeStatusLbl.ForeColor = System.Drawing.Color.Green;
        SubscribeStatusLbl.Text = "Selected patient subscribed successfully";

        try
        {

            subs.SubscribePatient();
        }
        catch (Exception ex)
        {
        
            SubscribeStatusLbl.Text =  ex.Message;
       
        }

    }

    protected void DropDownList2_TextChanged(object sender, EventArgs e)
    {
        TextBoxSelectedPatientID.Text = DropDownList2.SelectedValue.ToString();
    }

    protected void LogOut(object sender, EventArgs e)
    {
        Session.RemoveAll();
        Response.Redirect("MainMenu.aspx");
    }
}