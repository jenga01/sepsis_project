﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="RegisterPatient.aspx.cs" Inherits="RegisterPatient" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="/bootstrap/jquery-1.11.3.min.js"></script>
        <script src="/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="/bootstrap/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" href="style.css" />
          <title>Sepsis disease warning project</title>
    </head>

    <body class="body" runat="server">
        <form id="form1" runat="server" class="form-signin">
            <nav class="navbar navbar-inverse">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">midt</a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="MainMenu.aspx">Home <span class="sr-only">(current)</span></a></li>
                            <li class="dropdown active">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Patient <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                <li><a href="RegisterPatient.aspx">Register patient</a></li>
                                <li><a href="SubscribePage.aspx">Subscribe patient</a></li>
                                <li><a href="NursePatientOverview.aspx">Patient overview</a></li>
                                <li><a href="UpdatePatient.aspx">Edit patient's info</a></li>
                            </ul>
                            </li>
                                <li><a href="TOKSCalcPage.aspx">Calculate critical TOKS</a></li>
                                <li><a href="Graph.aspx">TOKS evolvement</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#">User ID: <asp:Label ID="Label9" runat="server" Text="Label"></asp:Label></a></li>
                            <li><asp:Button ID="Button1" runat="server" OnClick="LogOut" Text="Logout" CssClass="btn btn-primary LogOutButton" /></li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="container">
                <div class="MainForm">
                    <asp:Label ID="NameLabel" runat="server" Text="Enter Patient's full name"></asp:Label>
                    <asp:TextBox ID="TextBoxFname" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:Label ID="LabelAddress" runat="server" Text="Enter patient's address"></asp:Label>
                    <asp:TextBox ID="TextBoxAddress" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:Label ID="LabelPhoneNr" runat="server" Text="Enter patient's phone number"></asp:Label>
                    <asp:TextBox ID="TextBoxPhoneNr" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:Label ID="Label5" runat="server" Text="Choose patient type:"></asp:Label>
                    <asp:RadioButtonList ID="RadioButtonList1" runat="server" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged" AutoPostBack="True">
                        <asp:ListItem Value="Regular">Regular</asp:ListItem>
                        <asp:ListItem Value="KOL">KOL</asp:ListItem>
                        <asp:ListItem Value="AFLI">AFLI</asp:ListItem>
                        <asp:ListItem>KOL&amp;AFLI</asp:ListItem>
                    </asp:RadioButtonList>
                    <asp:Label ID="LabelBirthDate" runat="server" Text="Patient's birth date"></asp:Label>
                    <asp:TextBox ID="TextBoxPatientBD" runat="server" TextMode="Date" CssClass="form-control"></asp:TextBox>
                    <div class="RegisterSubmit">
                        <asp:Button ID="RegPatientBtn" runat="server" Text="Register Patient" OnClick="Button1_Click" CssClass="btn btn-success" />
                    <asp:Label ID="RegisterStatusLbl" runat="server" Font-Bold="True" Font-Size="X-Large" Text="Label" Visible="False"></asp:Label>
                    </div>
                </div>
            </div>
        </form>
    </body>
</html>
