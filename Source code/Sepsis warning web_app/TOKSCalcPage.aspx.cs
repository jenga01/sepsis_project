﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class TOKSCalcPage : System.Web.UI.Page
{
    private BLLTOKS tks = new BLLTOKS();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["NurseID"] == null) //will redirect immediately if not loged in before
        {
            Response.Redirect("LoginPage.aspx");
        }
        else
        {
            SessionLbl.Text = Session["NurseID"].ToString();



        }
        if (!IsPostBack)
        {
            TextBoxSatLvlKOL.Enabled = true;
            TextBoxSatLvl.Enabled = false;
            TextBoxPulse.Enabled = true;
            TextBoxPulsAFLI.Enabled = false;
        }


     //  test.Text = tks.testc();

    }

    protected void LogOut(object sender, EventArgs e)
    {
        Session.RemoveAll();
        Response.Redirect("MainMenu.aspx");
    }

    protected void Button1_Click(object sender, EventArgs e)
    {




        tks.PatientId = int.Parse(TextBoxPatientID.Text);
        tks.RespFreq = int.Parse(TextBoxRespFreq.Text);

        tks.O2SatLvl = int.Parse(TextBoxSatLvlKOL.Text);
        tks.O2SatLvlKOL = int.Parse(TextBoxSatLvl.Text);
        tks.SysBloodPress = int.Parse(TextBoxBloodPress.Text);
        tks.Puls = int.Parse(TextBoxPulse.Text);
        tks.PulsAFLI = int.Parse(TextBoxPulsAFLI.Text);
        tks.Temperature = double.Parse(TextBoxTemperature.Text);
        tks.ConsLvl = int.Parse(ListBoxConsLvl.Text);




        tks.CalculateRespFrequency();
        tks.CalculateO2Sat();
        tks.CalculateO2SatKol();
        tks.CalculateSBT();
        tks.CalculatePuls();
        tks.CalculatePulsAFLI();
        tks.CalculateTemp();
        tks.consLevel();
        tks.TOKSAlert();
      

       Response.Redirect("NursePatientOverview.aspx");

    }



    protected void CheckBox2_CheckedChanged(object sender, EventArgs e)
    {


        if (CheckBox2.Enabled == true)

            TextBoxSatLvl.Enabled = CheckBox2.Checked;
        TextBoxSatLvlKOL.Enabled = false;

        if (CheckBox2.Checked == false)
            TextBoxSatLvlKOL.Enabled = true;





    }
    protected void CheckBox3_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckBox3.Enabled == true)

            TextBoxPulsAFLI.Enabled = CheckBox3.Checked;
        TextBoxPulse.Enabled = false;

        if (CheckBox3.Checked == false)
            TextBoxPulse.Enabled = true;

    }
    protected void DropDownList1_TextChanged(object sender, EventArgs e)
    {
        TextBoxPatientID.Text = DropDownList1.SelectedValue.ToString();
    }

   

}