﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;


public partial class TestPAGE : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["NurseID"] == null) //will redirect immediately if not loged in before
        {
            Response.Redirect("LoginPage.aspx");
        }
        else 
        {
            Label2.Text = Session["NurseID"].ToString();
        }
        Chart1.Series[0].XValueType = ChartValueType.DateTime;
        Chart1.ChartAreas[0].AxisX.LabelStyle.Format = "yyyy-MM-dd H:mm";
        Chart1.ChartAreas[0].AxisX.IntervalType = DateTimeIntervalType.Hours;
        Chart1.ChartAreas[0].AxisX.Interval = 12;
       //  Chart1
    }




    protected void DropDownList1_TextChanged(object sender, EventArgs e)
    {
        TextBox1.Text = DropDownList1.SelectedValue.ToString(); //hidden
    }

        protected void LogOut(object sender, EventArgs e)
    {
        Session.RemoveAll();
        Response.Redirect("MainMenu.aspx");
    }
}