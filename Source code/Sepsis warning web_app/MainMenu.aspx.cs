﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MainMenu : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        TimeSpan spanHour = TimeSpan.FromHours(1);

        //if (TimeSpan.Parse(Label3.Text) < spanHour)
        //{
        //    ListBox1.Items[0].Attributes.Add("style", "color:blue");
        //}

        if (Session["NurseID"] == null) //will redirect immediately if not loged in before
        {
            Response.Redirect("LoginPage.aspx");
        }
        else 
        {
            Label1.Text = Session["NurseID"].ToString();
           

        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
       Session.Remove("NurseID");
       if (Session["NurseID"] == null)
        {
            Response.Redirect("LoginPage.aspx");
        }
    }
    protected void Button2_Click(object sender, EventArgs e)
    {

        Label1.Text = Session["NurseID"].ToString();
        Response.Redirect("RegisterPatient.aspx");
    }
    protected void Button1_Click1(object sender, EventArgs e)
    {
        Response.Redirect("RegisterPatient.aspx");
    }
    protected void Button2_Click1(object sender, EventArgs e)
    {
        Response.Redirect("TOKSCalcPage.aspx");
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        Response.Redirect("SubscribePage.aspx");
    }
    protected void Button6_Click(object sender, EventArgs e)
    {
        Response.Redirect("Graph.aspx");
    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        Response.Redirect("NursePatientOverview.aspx");
    }
    protected void Button5_Click(object sender, EventArgs e)
    {
        Response.Redirect("UpdatePatient.aspx");
    }
    protected void LogOut(object sender, EventArgs e)
    {
        Session.RemoveAll();
        Response.Redirect("MainMenu.aspx");
    }
    protected void ListBox1_TextChanged(object sender, EventArgs e)
    {
        Listbox1SelectedValueLbl.Text = ListBox1.SelectedValue.ToString();
    }
    protected void ListBox2_TextChanged(object sender, EventArgs e)
    {
        ListBox2SelectedValue.Text = ListBox2.SelectedValue.ToString();

        DateTime tr = DateTime.Parse(ListBox2SelectedValue.Text);
        TimeSpan sincelast = tr - DateTime.Now;
        TimeSpan ts = new TimeSpan(0, 0, 0);
        TimeRemLbl.Text = DateTime.Now.ToString("yyyy-MM-dd HH:mm");
        TimeRemLbl.Text = sincelast.ToString();

        if (sincelast < ts)
        {
            TimeRemLbl.Visible = true;
            Label5.Visible = false;
            TimeRemLbl.ForeColor = System.Drawing.Color.Red;
            TimeRemLbl.Text = "Completed";

        }
        else
        {
            TimeRemLbl.Visible = true;
            Label5.Visible = true;
            TimeRemLbl.ForeColor = System.Drawing.Color.Green;
        }
    }
}
