﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="NursePatientOverview.aspx.cs" Inherits="NursePatientOverview" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script type="text/javascript" src="/bootstrap/jquery-1.11.3.min.js"></script>
        <script src="/bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="/bootstrap/css/bootstrap-theme.min.css" />
        <link rel="stylesheet" href="style.css" />
          <title>Sepsis disease warning project</title>
    </head>

    <body class="body" runat="server">
        <form id="form1" runat="server" class="form-signin">
            <nav class="navbar navbar-inverse">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#">midt</a>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="MainMenu.aspx">Home <span class="sr-only">(current)</span></a></li>
                            <li class="dropdown active">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Patient <span class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                <li><a href="RegisterPatient.aspx">Register patient</a></li>
                                <li><a href="SubscribePage.aspx">Subscribe patient</a></li>
                                <li><a href="NursePatientOverview.aspx">Patient overview</a></li>
                                <li><a href="UpdatePatient.aspx">Edit patient's info</a></li>
                            </ul>
                            </li>
                                <li><a href="TOKSCalcPage.aspx">Calculate critical TOKS</a></li>
                                <li><a href="Graph.aspx">TOKS evolvement</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="#">User ID: <asp:Label ID="SessionLbl" runat="server" Text="Label"></asp:Label></a></li>
                            <li><asp:Button ID="Button1" runat="server" OnClick="LogOut" Text="Logout" CssClass="btn btn-primary LogOutButton" /></li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="container">
                <div class="MainForm">
                    <p>Select my Patient:</p>
                    <asp:DropDownList CssClass="form-control" ID="DropDownList1" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource1" DataTextField="FullName" DataValueField="Patient_Id" OnTextChanged="DropDownList1_TextChanged">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:BachelorDBConnectionString %>" SelectCommand="SELECT PatientSubscribe.Patient_Id, PatientSubscribe.Nurse_Id, Patient.FullName FROM PatientSubscribe INNER JOIN Patient ON PatientSubscribe.Patient_Id = Patient.Patient_Id WHERE (PatientSubscribe.Nurse_Id = @Nurse_Id) ">
                    <SelectParameters>
                    <asp:SessionParameter Name="Nurse_Id" SessionField="NurseID" />
                    </SelectParameters>
                    </asp:SqlDataSource>
                    <p>Select measurement:</p>
                    <asp:DropDownList ID="DropDownList2" CssClass="form-control" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource3" DataTextField="Last_Measurement_Time" DataValueField="Next_Measurement_Time" OnTextChanged="DropDownList2_TextChanged">
                    </asp:DropDownList>
                    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:BachelorDBConnectionString %>" SelectCommand="SELECT TOKS.Patient_Id, TOKS.Last_Measurement_Time, TOKS.Next_Measurement_Time, Patient.FullName FROM TOKS INNER JOIN Patient ON TOKS.Patient_Id = Patient.Patient_Id WHERE (TOKS.Patient_Id = @Patient_Id)">
                    <SelectParameters>
                    <asp:ControlParameter ControlID="DropDownList1" Name="Patient_Id" PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                    </asp:SqlDataSource>
                    <p>Remaining time until next measurement:<asp:Label ID="Label3" runat="server" Text=""></asp:Label>
                    </p>
                    <asp:Label ID="Label2" runat="server" Text="Label" Visible="false"></asp:Label>
                    <asp:DataList ID="DataList1" runat="server" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataSourceID="SqlDataSource2" ForeColor="Black" GridLines="Vertical">
                    <AlternatingItemStyle BackColor="White" />
                    <FooterStyle BackColor="#CCCC99" />
                    <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
                    <ItemStyle BackColor="#F7F7DE" />
                    <ItemTemplate>
                    Patient_Id:
                    <asp:Label ID="Patient_IdLabel" runat="server" Text='<%# Eval("Patient_Id") %>' />
                    <br />
                    TOKS_Score:
                    <asp:Label ID="TOKS_ScoreLabel" runat="server" Text='<%# Eval("TOKS_Score") %>' />
                    <br />
                    Alarm:
                    <asp:Label ID="AlarmLabel" runat="server" Text='<%# Eval("Alarm") %>' />
                    <br />
                    Last_Measurement_Time:
                    <asp:Label ID="Last_Measurement_TimeLabel" runat="server" Text='<%# Eval("Last_Measurement_Time") %>' />
                    <br />
                    Next_Measurement_Time:
                    <asp:Label ID="Next_Measurement_TimeLabel" runat="server" Text='<%# Eval("Next_Measurement_Time") %>' />
                    <br />
                    </ItemTemplate>
                    <SelectedItemStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
                    </asp:DataList>
                    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:BachelorDBConnectionString %>" SelectCommand="SELECT [Patient_Id], [TOKS_Score], [Alarm], [Last_Measurement_Time], [Next_Measurement_Time] FROM [TOKS] WHERE ([Patient_Id] = @Patient_Id)">
                    <SelectParameters>
                    <asp:ControlParameter ControlID="DropDownList1" Name="Patient_Id" PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                    </asp:SqlDataSource>

    
                </div>
            </div>
        </form>
    </body>
</html>
